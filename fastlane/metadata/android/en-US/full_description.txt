An unofficial APKMirror client/web app (forked from the original, which is now abandoned).


Longer Description:

An Android app that utilizes a WebView to browse APKMirror.
APKMirror provides APKs, as the name obviously suggests.
This app saves you the trouble of having to open up a browser and visit APKMirror by typing the URL,
and is the sole purpose of this app existing (because who needs stupid boring browsers when
you can create an entire app for a site, amirite?).


Features:

- Quick loading (depends on phone; newer models load much faster)
- Ability to choose any download manager
- Clean-ish material design
- Small-ish APK size (remember to clear the cache regularly, as it builds up when downloading APKs)


Things that would constitute as Anti-Features (that aren't already shown by F-Droid):

- The app itself does NOT contain any ad libraries whatsoever (it is completely FOSS).
However, as anyone who has visited the APKMirror site probably knows, they do display ads.
As this app utilizes a WebView, the site's ads will also end up being displayed in the app.
Remember that they show ads to be able to keep their site up, so try not to think too harshly
of them.
